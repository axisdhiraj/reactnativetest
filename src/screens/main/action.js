import axios from 'axios';
import ActionNames from '../../utils/actionNames';
export const updateFolderListing = (key, value) => ({
  type: ActionNames.FOLDER_LISTING,
  payload: { [key]: value }
});

/**
 * set access token and domain in axios instance header
 */
export const getFolderListing = () => {
  return async (dispatch, getState) => {
   let requestOptions = {
      headers: {
        'Authorization' : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvZG9jdG9yLmNsaXJuZXQuY29tIiwiYXVkIjoiaHR0cHM6XC9cL2RvY3Rvci5jbGlybmV0LmNvbSIsInVzZXJkZXRhaWwiOnsidXNlcl9tYXN0ZXJfaWQiOiI4OCIsInVzZXJuYW1lIjoiOTczMzIyMzEyMSIsImZpcnN0X25hbWUiOiJzYXlhbiIsImxhc3RfbmFtZSI6ImJhbmVyamVlIiwiY2xpZW50IjoiMSIsImNsaWVudF9pZHMiOiIxIiwiZ3JvdXBfaWRzIjowfSwiaWF0IjoxNjQ4ODE3NDYzLCJleHAiOjE2ODAzNTM0NjN9.W2JHt92bmxTJhs93OcwXYV2yjloJ7u36H0b-aYFnBIo"
      }
    };
    axios.get("https://developer.clirnet.com/knowledge/rnv21/epub/listing?from=0&to=10", requestOptions)
    .then((response) => {
      console.log(response)
      if(response.status == 200){
        dispatch(updateFolderListing('data', response?.data?.data))
      }
     
    })
    .catch( (error)=> {
      console.log(error)
    });
  }
}
export const getDetails = (type_id,navigation) => {
  return async (dispatch, getState) => {
   let requestOptions = {
      headers: {
        'Authorization' : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvZG9jdG9yLmNsaXJuZXQuY29tIiwiYXVkIjoiaHR0cHM6XC9cL2RvY3Rvci5jbGlybmV0LmNvbSIsInVzZXJkZXRhaWwiOnsidXNlcl9tYXN0ZXJfaWQiOiI4OCIsInVzZXJuYW1lIjoiOTczMzIyMzEyMSIsImZpcnN0X25hbWUiOiJzYXlhbiIsImxhc3RfbmFtZSI6ImJhbmVyamVlIiwiY2xpZW50IjoiMSIsImNsaWVudF9pZHMiOiIxIiwiZ3JvdXBfaWRzIjowfSwiaWF0IjoxNjQ4ODE3NDYzLCJleHAiOjE2ODAzNTM0NjN9.W2JHt92bmxTJhs93OcwXYV2yjloJ7u36H0b-aYFnBIo"
      }
    };
    axios.get("https://developer.clirnet.com/knowledge/rnv21/epub/detail?type_id="+type_id, requestOptions)
    .then((response) => {
      console.log(response)
      if(response.status == 200){
       dispatch(updateFolderListing('details', response?.data?.data))
       navigation.push("Details")
      }
     
    })
    .catch( (error)=> {
      console.log(error)
    });
  }
}
