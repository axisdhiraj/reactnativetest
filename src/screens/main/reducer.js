//Custom Imports
import ActionNames from "../../utils/actionNames";
export const initialFolderState = {
    name: 'wwwww',
    data: [],
    details:{}
}

export const folderReducer = (state = initialFolderState, action) => {
    switch (action.type) {
        case ActionNames.FOLDER_LISTING:
            return { ...state, ...action.payload }
        default:
            return state;
    }
    
};