import {
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  FlatList,
  TouchableHighlight,
  Image,
} from 'react-native';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  updateFolderListing,
  getFolderListing,
  getDetails
} from './action';
import { vw, vh,screenHeight } from '../../utils/dimesnion';
class MainScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    
    };
  
  }

  componentDidMount() { 
    this.props.getFolderListing();
   this.props.updateFolderListing("name","Dhiraj Saha")
  }

  componentWillUnmount() {

  }
  handlePressListItem(item) {
    console.log(item)
    this.props.getDetails(item.type_id,this.props.navigation)
   /// this.props.navigation.push('Details')
  }
  renderItem = ({item}) => (
    <TouchableHighlight
      onPress={() => this.handlePressListItem(item)}
      underlayColor={"#F2F2F2"}
      activeOpacity={0.5}>
      <View style={{flex: 1,
        flexDirection: 'column',
        margin: vh(10)}}>
        <Image
            style={styles.profileImageStyle}
            source={{uri: item.image}}
            resizeMode={'cover'}
          />
      </View>
    </TouchableHighlight>
  );
  render() {
    return (
      <SafeAreaView style={styles.MainContainer}>
        <View style={styles.HeaderView}>
          <Text style={styles.HeaderText}>Listing Screen</Text>
        </View>
        <View style={styles.BodyView}>
        <FlatList
          data={this.props.data}
          renderItem={this.renderItem}
          contentContainerStyle={{marginTop: vh(10)}}
          keyExtractor={(item) => String(item.type_id)}
          numColumns={3}
        />
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  let {
    name,
    data,
  } = state.folderReducer;
  return {
    name,
    data,
  };
};
const mapDispatchToProps = (dispatch) => ({
  updateFolderListing: (key, value) =>
  dispatch(updateFolderListing(key, value)),
  getFolderListing:() => dispatch(getFolderListing()),
  getDetails:(type_id,navigation) => dispatch(getDetails(type_id,navigation)),
  dispatch,
});
export default connect(mapStateToProps, mapDispatchToProps)(MainScreen);
const styles = StyleSheet.create({
  MainContainer: {
    backgroundColor:'#FFFFFF'
  },
  HeaderView: {
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:'#5a4d94', 
    height:vh(50)
  },
 BodyView: {
    paddingLeft:vh(10),
    paddingRight:vh(10),
    minHeight:screenHeight
  },
  HeaderText: {
    color:'#FFFFFF',
    fontSize: vh(20)
   },
   profileImageStyle: {
    width: vw(100),
    height: vw(100),
  },
  BodyText: {
    color:'#000000',
    fontSize: vh(15),
    //fontWeight:'bold'
   },
   
});