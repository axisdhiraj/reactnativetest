import {
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  FlatList,
  TouchableHighlight,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import Pdf from 'react-native-pdf';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  updateFolderListing,
  getFolderListing
} from './action';
import { vw, vh,screenHeight,screenWidth } from '../../utils/dimesnion';
import { hidden } from 'ansi-colors';

const back = require("../../assets/back.png");
class DetailsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      numberOfPages: 0,
    };
    this.pdf = null;
  
  }

  componentDidMount() { 

  }

  componentWillUnmount() {
 
  }
  _handleBackPress = () => {
    this.props.navigation.pop();
   };
   _prev = () => {
    if(this.state.page>1){
      let prePage = (this.state.page-1)
      this.setState({
        page: prePage
    });
    this.pdf.setPage(prePage);
    }
   };
   _next = () => {
    if(this.state.page<this.state.numberOfPages){
      let prePage = (this.state.page+1)
      this.setState({
        page: prePage
    });
    this.pdf.setPage(prePage);
    }
   };
  render() {
    const source = { uri: this.props.details.epub_file, cache: true };
    return (
      <SafeAreaView style={styles.MainContainer}>
      
        <View style={styles.HeaderView}>
        <TouchableOpacity style={{paddingLeft:vh(10)}}  onPress={this._handleBackPress} >
              <Image style={styles.backIcon} source={back} />
            </TouchableOpacity>
          <Text style={styles.HeaderText}>Details Screen</Text>
          <View></View>
        </View>
        <ScrollView>
        <View style={styles.BodyView}>
          <View style={styles.SemiHead}>
          <View style={{paddingBottom:vh(10),flexDirection:'row'}}>
          
            <Text style={[styles.BodyText,{fontWeight:'bold'}]}>Title: </Text><Text style={styles.BodyText}>{this.props.details.title}</Text>
            <View></View>
          </View>
          <View style={{flexDirection:'row'}}>
            <Text style={[styles.BodyText,{fontWeight:'bold'}]}>Sponsor Name: </Text><Text style={styles.BodyText}>{this.props.details.sponsor_name }</Text>
          </View>
          </View>
          <View>
        <Pdf
       // singlePage = {true}
        ref={(pdf) => {
          this.pdf = pdf;
        }}
                    source={source}
                    onLoadComplete={(numberOfPages) => {
                      this.setState({
                         numberOfPages: numberOfPages 
                      });
                      console.log(`total page count: ${numberOfPages}`);
                  }}
                    onPageChanged={(page,numberOfPages) => {
                      this.setState({
                        page: page
                    });
                        console.log(`Current page: ${page}`);
                    }}
                    onError={(error) => {
                        console.log(error);
                    }}
                    onPressLink={(uri) => {
                        console.log(`Link pressed: ${uri}`);
                    }}
                    style={styles.pdf}/>
                    <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-between', margin:vh(10)}}>
                      <TouchableOpacity disabled={this.state.page == 1} onPress={this._prev} style={{ backgroundColor:'#5a4d94', padding:vh(10) }}>
                        <Text style={styles.ButtonText}>Previous</Text>
                      </TouchableOpacity>
                      <View>
                        <Text style={styles.ButtonTextBlack}>Page {this.state.page} of {this.state.numberOfPages}</Text>
                      </View>
                      <TouchableOpacity disabled={this.state.page == this.state.numberOfPages}  onPress={this._next} style={{ backgroundColor:'#5a4d94', padding:vh(10) }}>
                        <Text style={styles.ButtonText}>Next</Text>
                      </TouchableOpacity>
                    </View>
                    </View>
        </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  let {
    name,
    data,
    details
  } = state.folderReducer;
  return {
    name,
    data,
    details
  };
};
const mapDispatchToProps = (dispatch) => ({
  updateFolderListing: (key, value) =>
  dispatch(updateFolderListing(key, value)),
  getFolderListing:() => dispatch(getFolderListing()),
  dispatch,
});
export default connect(mapStateToProps, mapDispatchToProps)(DetailsScreen);
const styles = StyleSheet.create({
  MainContainer: {
    backgroundColor:'#FFFFFF'
  },
  HeaderView: {
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',
    backgroundColor:'#5a4d94', 
    height:vh(50)
  },
 BodyView: {
    paddingLeft:vh(10),
    paddingRight:vh(10),
    minHeight:screenHeight
  },
  SemiHead: {
    padding:vh(10),

    backgroundColor:"#d0cde0",
    marginBottom:vh(10),
    marginTop:vh(10)

  },
  HeaderText: {
    color:'#FFFFFF',
    fontSize: vh(20)
   },
   ButtonText: {
    color:'#FFFFFF',
    fontSize: vh(17),
    fontWeight:'bold'
   },
   ButtonTextBlack: {
    color:'#000000',
    fontSize: vh(17),
    fontWeight:'bold'
   },
  BodyText: {
    color:'#000000',
    fontSize: vh(15),
    //fontWeight:'bold'
   },
   backIcon: {
    width: vw(25),
    height: vw(25),
    tintColor: 'white',
    
  },
   pdf: {
    flex:1,
    paddingRight:vh(10),
    width:screenWidth-vh(20),
    height:screenHeight-vh(300),
    overflow:"hidden",
}
   
});