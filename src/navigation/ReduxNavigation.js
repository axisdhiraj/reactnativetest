import React from 'react';
import {connect} from 'react-redux';
import AppNavigator from './appRouteConfig';
import {createReduxContainer} from 'react-navigation-redux-helpers';

const App = createReduxContainer(AppNavigator);

class ReduxNavigation extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    
  }

  componentWillUnmount() {
   
  }

  

  render() {
    const {nav, dispatch} = this.props;
    return <App state={nav} dispatch={dispatch} />;
  }
}

const mapStateToProps = (state) => ({
  nav: state.nav,
});


export default connect(mapStateToProps)(ReduxNavigation);