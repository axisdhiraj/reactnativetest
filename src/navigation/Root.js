import React, {useEffect} from 'react';
import {Provider} from 'react-redux';
import ReduxNavigation from './ReduxNavigation';
import {store} from '../../src/store';

export default function Root() {
 useEffect(() => {

  }, []);

  return (
    <Provider store={store}>
      <ReduxNavigation />
    </Provider>
  );
}
