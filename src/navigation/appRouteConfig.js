/* eslint-disable no-trailing-spaces */
import { createStackNavigator, CardStyleInterpolators } from 'react-navigation-stack';
import Main from '../../src/screens/main/Main';
import Details from '../../src/screens/main/Details';

const RootStack = createStackNavigator(
  {
    Main: {
      screen: Main,
      navigationOptions: { headerShown: false },
    },
    Details: {
      screen: Details,
      navigationOptions: { headerShown: false },
    },
  },
  {
    initialRouteName: 'Main',
    defaultNavigationOptions: {
      cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
    }
  },
);

export default RootStack;
