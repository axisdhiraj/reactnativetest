import thunk from 'redux-thunk';
import { createNavigationReducer } from 'react-navigation-redux-helpers';
import { combineReducers, compose, createStore, applyMiddleware } from 'redux';
//Custom Imports
import AppNavigator from '../navigation/appRouteConfig';
import { folderReducer} from '../screens/main/reducer';
import { createLogger } from 'redux-logger'
const navReducer = createNavigationReducer(AppNavigator);
const appReducer = combineReducers({
  nav: navReducer,
  folderReducer,
});

const rootReducer = (state, action) => {
  return appReducer(state, action);
};
const enhancer = compose(
  applyMiddleware(
    thunk,
    createLogger({
      predicate: () => __DEV__,
    }),
  ),
);
export const store = createStore(rootReducer, {}, enhancer);
