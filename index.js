/**
 * @format
 */
import Root from './src/navigation/Root';
import { AppRegistry, LogBox } from 'react-native';
import { name as appName } from './app.json';

LogBox.ignoreAllLogs();
AppRegistry.registerComponent(appName, () => Root);
// import {AppRegistry} from 'react-native';
// import App from './App';
// import {name as appName} from './app.json';

// AppRegistry.registerComponent(appName, () => App);