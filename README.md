
# Interview Assessment Test #

A react native PDF view component (cross-platform support)

### Feature

* Listing Screen image only in grid view
* Details Screen
* Data store in redux
* PDF Viewer
* Zoom In/ Zoom Out in PDF
* PDF Scroll also 
* Previous and Next Button in PDF


### Screen Shot
![PNG](Listing.png)
![PNG](Details.png)
